import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Input from '../components/Forms/Input';

import { BaseSkin } from '../assets/base/skin';
import { BaseHair } from '../assets/base/hair';

class BaseParts extends Component {
  render() {
    return <div className="uk-card uk-card-default uk-card-body">
      <div className="" data-uk-grid>
        <div className="uk-width-1-6">

          <div className="uk-margin">
            <button className="uk-button uk-button-default uk-width-1-1">Feminine</button>
          </div>
          <div className="uk-margin">
            <button className="uk-button uk-button-default uk-width-1-1">Masculine</button>
          </div>
          <div className="uk-margin">
            <button className="uk-button uk-button-default uk-width-1-1">Androgynous</button>
          </div>

        </div>
        <div className="uk-width-5-6">

          <ul data-uk-tab="connect: #base-body-tab; animation: uk-animation-fade">
            <li><a href="#">Skin</a></li>
            <li><a href="#">Face</a></li>
            <li><a href="#">Hair</a></li>
            <li><a href="#">Beard</a></li>
            <li><a href="#">Ears</a></li>
            <li><a href="#">Eyes</a></li>
            <li><a href="#">Eyebrows</a></li>
            <li><a href="#">Nose</a></li>
            <li><a href="#">Mouth</a></li>
          </ul>

          <ul id="base-body-tab" className="uk-switcher uk-margin">
            <li>
              <div className="uk-flex uk-flex-between uk-text-center" data-uk-grid>
                {BaseSkin.map((item, index) => <div key={index}>
                  <label>
                    {item.images && <div>
                      <img src={item.images.body} alt={item.name} />
                    </div>}
                    <div className="uk-text-small">
                      {item.name}
                    </div>
                    <Field component={Input} name={item.input_name} type="radio" value={item.id} />
                  </label>
                </div>)}
              </div>
            </li>
            <li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur, sed do eiusmod.</li>
            <li>
              <div className="uk-flex uk-flex-between uk-text-center" data-uk-grid>
                {BaseHair.map((item, index) => <div key={index}>
                  <label>
                    {item.images && <div>
                      <img src={item.images.body} alt={item.name} />
                    </div>}
                    <div className="uk-text-small">
                      {item.name}
                    </div>
                    <Field component={Input} name={item.input_name} type="radio" value={item.id} />
                  </label>
                </div>)}
              </div>
            </li>
            <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
            <li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur, sed do eiusmod.</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
            <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
            <li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur, sed do eiusmod.</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
          </ul>

        </div>
      </div>
    </div>
  }
}

BaseParts = reduxForm({
  form: 'baseForm',
  enableReinitialize: true
})(BaseParts);

BaseParts = connect(
  state => {
    return {
      initialValues: {
        base_skin: BaseSkin[0].id
      }
    }
  }
)(BaseParts)

export default BaseParts;