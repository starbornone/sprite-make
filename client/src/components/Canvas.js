import React, { Component } from 'react';

class Canvas extends Component {
  componentDidMount() {
    const canvas = this.refs.canvas
    const ctx = canvas.getContext("2d")
    const img = this.refs.image

    img.onload = () => ctx.drawImage(img, 0, 0)
  }

  render() {
    const { height, image, width } = this.props;

    return <div>
      <canvas ref="canvas" width={width} height={height} style={{ border: '1px solid #999' }} />
      <img ref="image" src={image} hidden />
    </div>
  }
}

export default Canvas