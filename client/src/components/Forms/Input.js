import React from 'react';

const Input = ({ className, input, label, meta: { touched, error }, name, placeholder, type }) => {
  let inputClass = '';
  switch (type) {
    case 'radio': {
      inputClass = 'uk-radio'
      break;
    }
    defatult:
      inputClass = 'uk-input'
      break;
  }

  return <div className={className}>
    <label>{label}</label>
    <input
      {...input}
      className={`{'uk-input'} uk-width-1-1 ${touched && error ? 'uk-form-danger' : ''}`}
      name={name}
      placeholder={placeholder}
      type={type}
    />
    {touched && error && <div className="uk-text-danger">{error}</div>}
  </div>
}

export default Input;