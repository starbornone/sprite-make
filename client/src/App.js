import React, { Component } from 'react';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';

import './App.css';

import Navbar from './hoc/Layout/Navbar';

import Canvas from './components/Canvas';

import BaseParts from './containers/BaseParts';
import EquipmentSelector from './containers/EquipmentSelector';

import { BaseSkin } from './assets/base/skin';
import { BaseHair } from './assets/base/hair';

import { ClothingHead } from './assets/clothing/head';
import { ClothingTorso } from './assets/clothing/torso';
import { ClothingLegs } from './assets/clothing/legs';
import { ClothingFeet } from './assets/clothing/feet';

import { ArmourTorso } from './assets/armour/torso';

class App extends Component {
  componentDidUpdate() {
    const { armour_torso, base_hair, base_skin, clothing_feet, clothing_head, clothing_legs, clothing_torso } = this.props;
    const canvas = this.refs.inpagecanvas;
    const ctx = canvas.getContext("2d");

    const bodyImage = base_skin ? BaseSkin[parseInt(base_skin, 10)].images.body : BaseSkin[0].images.body;

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    this.updateCanvas(bodyImage, 0, 0);
    if (clothing_head) {
      this.updateCanvas(ClothingHead[parseInt(clothing_head, 10)].images.body, 0, 0);
    }
    if (clothing_feet) {
      this.updateCanvas(ClothingFeet[parseInt(clothing_feet, 10)].images.body, 0, 0);
    }
    if (clothing_legs) {
      this.updateCanvas(ClothingLegs[parseInt(clothing_legs, 10)].images.body, 0, 0);
    }
    if (clothing_torso) {
      this.updateCanvas(ClothingTorso[parseInt(clothing_torso, 10)].images.body, 0, 0);
    }
    if (armour_torso) {
      this.updateCanvas(ArmourTorso[parseInt(armour_torso, 10)].images.body, 0, 0);
    }
    if (base_hair) {
      this.updateCanvas(BaseHair[parseInt(base_hair, 10)].images.body, 0, 0);
    }
  }

  updateCanvas(image, x, y) {
    const canvas = this.refs.inpagecanvas;
    const ctx = canvas.getContext("2d");

    let img = new Image();
    img.src = image;
    img.onload = () => ctx.drawImage(img, x, y);
  }

  render() {
    return <div className="App">
      <Navbar />

      <div className="uk-container">
        <BaseParts />

        <div className="uk-grid-small uk-grid-match uk-margin" data-uk-grid>
          <div className="uk-width-1-5">
            <div className="uk-card uk-card-default uk-card-body uk-margin-bottom uk-flex uk-flex-middle uk-flex-center uk-text-center">
              <canvas ref="inpagecanvas" height="48" width="48" />
            </div>

            <div className="uk-card uk-card-default uk-card-body uk-margin-bottom uk-flex uk-flex-middle uk-flex-center uk-text-center">
              <Canvas height="48" image={BaseSkin[0].images.body} width="48" />
            </div>
          </div>
          <div className="uk-width-4-5">
            <div className="uk-card uk-card-default uk-card-body uk-margin-left">
              <div className="uk-grid-small uk-child-width-1-3 uk-margin" data-uk-grid>
                <div>
                  <button className="uk-button uk-button-secondary uk-width-1-1">Save</button>
                </div>
                <div>
                  <button className="uk-button uk-button-primary uk-width-1-1">Export</button>
                </div>
                <div>
                  <button className="uk-button uk-button-danger uk-width-1-1">Clear</button>
                </div>
              </div>

              <div className="uk-panel-scrollable uk-height-medium">
                Sheet goes here
              </div>
            </div>
          </div>
        </div>

        <EquipmentSelector />

      </div>
    </div>
  }
}

const baseSelector = formValueSelector('baseForm');
const equipmentSelector = formValueSelector('equipmentForm');

App = connect(
  state => {
    const armour_torso = equipmentSelector(state, 'armour_torso');
    const base_hair = baseSelector(state, 'base_hair');
    const base_skin = baseSelector(state, 'base_skin');
    const clothing_head = equipmentSelector(state, 'clothing_head');
    const clothing_legs = equipmentSelector(state, 'clothing_legs');
    const clothing_torso = equipmentSelector(state, 'clothing_torso');
    const clothing_feet = equipmentSelector(state, 'clothing_feet');
    return {
      armour_torso,
      base_hair,
      base_skin,
      clothing_head,
      clothing_legs,
      clothing_torso,
      clothing_feet
    }
  }
)(App);

export default App;