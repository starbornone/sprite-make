import Empty from '../../images/empty.png';

const inputName = 'base_skin';

export const BaseSkin = [{
  id: '0',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Body_-_Fair_-_Idle.png'
  },
  input_name: inputName,
  name: 'Light'
}, {
  id: '1',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Body_-_Dark_-_Idle.png'
  },
  input_name: inputName,
  name: 'Dark'
}];