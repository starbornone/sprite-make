import Empty from '../../images/empty.png';

const inputName = 'clothing_legs';

export const ClothingLegs = [{
  id: '0',
  images: {
    face: Empty,
    body: Empty
  },
  input_name: inputName,
  name: 'None'
}, {
  id: '1',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574439/spritemake/Pants_-_Linen_-_Idle.png'
  },
  input_name: inputName,
  name: 'Linen Pants'
}];