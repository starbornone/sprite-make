const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SheetSchema = new Schema({
  name: {
    required: true,
    type: String
  },
  subcategory: {
    type: Schema.Types.ObjectId,
    ref: 'subcategories'
  }
})

module.exports = Sheet = mongoose.model('sheet', SheetSchema)