const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SubcategorySchema = new Schema({
  level: {
    required: true,
    type: Number
  },
  name: {
    required: true,
    type: String
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'categories'
  }
})

module.exports = Subcategory = mongoose.model('subcategory', SubcategorySchema)