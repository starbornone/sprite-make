const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Sheet = require('../../models/Sheet')

// @route   GET api/sheets
// @desc    Get sheets
// @access  Public
router.get('/', (req, res) => {
  Sheet.find()
    .then(sheets => res.json(sheets))
    .catch(err => res.status(404).json({ nosheetsfound: 'No sheets found' }))
})

module.exports = router